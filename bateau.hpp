/*
 * Cours:       INFO4008 : Programmation UNIX
 * 
 * Fichier d'entete des fonctions qui nous permettrons de vérifier les coordonnées entré par l'utilisateur
 * Ou de générer automatiquement des coordonées valides qui seront utilisées par l'ordinateur (Processus)
 */
#ifndef BATEAU_H
#define BATEAU_H

#include <cstdlib>
#include <string>
#include <vector>
#include <iostream>

#include "coordonnees.hpp"

/*
    Définition de la Classe Bateau ...
*/

class Bateau
{
public:
	Bateau(void);
	// ici le passage par référence de string peut etre
	// remplacer par string_view(c++17)
	Bateau(const std::string& alias_bateau, const int taille_bateau, const bool peut_etre_modifier);
    virtual ~Bateau(void);                              //dsteur

	//Accesseurs et modificateurs
	std::string getType();
	void setType(const std::string& type_bateau);
	std::string getAlias();
	void setAlias(const std::string& alias_bateau);
	int getTaille();
	void setTaille(const int taille_bateau);
	std::string getCoordonnees();
	void setCoordonnees(const std::string& coordonnees_bateau);
	bool getModifier();
	void setModifier(const bool peut_etre_modifier);
	
	// méthode de la classe Bateau
	std::vector<short>coordonneesConverties();// méthodes qui va renvoyé les coordonnées convertit en tableau d'entier pour être utilisé sur la grille
	void genererCoordonnees();				// méthode qui va générer automatiquement les coordonnées d'un bateau en fonction de sa taille
	void details();

	static short tailleCalculee(const std::string& coordonnees_bateau);// va vérifier si la taille est exacte en fonction des entrées des coordonnées du coté utiliseur;


private:
	std::string m_type_bateau;
	std::string m_alias_bateau;
	int m_taille_bateau;
	bool m_peut_etre_modifier;

	Coordonnees *m_coordonnees;	// va nous permettre de faire des manipulations des coordonnées(conversion, verification taille, génération des coordonnées)
};

#endif