/*
 * Cours:       INFO4008 : Programmation UNIX
 * 
 * Fichier contenant les fonctions qui nous permettrons de vérifier de construire nos bateaux
 * Ou de générer automatiquement des bateaux valides qui seront utilisées par l'ordinateur (Processus)
 */
#include "bateau.hpp"

//#include "coordonnees.cpp"

//using namespace std;
/*
 *  @name  :
 *  @param :
 *  @return:
 *  @desc  : initialisation du csteur
*/
//std::srand( time( NULL ) ); 

Bateau::Bateau()
{
    //srand( time( NULL ) );  
    //this->m_nom = "";
    this->m_type_bateau = "";
    this->m_alias_bateau = "";
    this->m_taille_bateau = 0;
    this->m_peut_etre_modifier = false;

    this->m_coordonnees = new Coordonnees();
}
/*
 *  @name  :
 *  @param :
 *  @return:
 *  @desc  : initialisation du csteur
*/
Bateau::Bateau(const std::string& alias_bateau, const int taille_bateau, const bool peut_etre_modifier)
{
    //srand( time( NULL ) );  

    this->m_alias_bateau = alias_bateau;
    this->m_taille_bateau = taille_bateau;
    this->m_peut_etre_modifier = peut_etre_modifier;
    // on crée un objet de type de coordonnees
    this->m_coordonnees = new Coordonnees();
}

/*
 *  @name  : 
 *  @param :
 *  @return:
 *  @desc  : Destructeur de la classe bateau
*/
Bateau::~Bateau(void){ 
    delete this->m_coordonnees;
}
/*
 *  @name  : 
 *  @param :
 *  @return:
 *  @desc  : Destructeur de la classe bateau
*/
std::string Bateau::getType()
{
    return this->m_type_bateau;
}
//
void Bateau::setType(const std::string& type_bateau)
{
    this->m_type_bateau = type_bateau;
}
//
std::string Bateau::getAlias()
{
    return this->m_alias_bateau;
}
//
void Bateau::setAlias(const std::string& alias_bateau)
{
    this->m_alias_bateau = alias_bateau;
}
//
int Bateau::getTaille()
{
    return this->m_taille_bateau;
}
//
void Bateau::setTaille(const int taille_bateau)
{
    this->m_taille_bateau = taille_bateau;
}
//
std::string Bateau::getCoordonnees()
{
    // ici on doit recupérer les coordonnées stockés
    // sous forme d'entier et les convertir en string
    // à partir de la classe coordonnées
    return this->m_coordonnees->getCoordonnees();
}
//
void Bateau::setCoordonnees(const std::string& coordonnees_bateau)
{
    // les coordonnées seront entrées en chaine de caractères
    // puis convertit en entier par la classe coordonnees
    // on peut calculer la taille du bateau
    this->m_coordonnees->setCoordonnees(coordonnees_bateau);
}
//
bool Bateau::getModifier()
{
    return this->m_peut_etre_modifier;
}
//
void Bateau::setModifier(const bool peut_etre_modifier)
{
    this->m_peut_etre_modifier = peut_etre_modifier;
}
//
void Bateau::details()
{
    std::cout << "Type de Bateau :\t" << this->getType() << std::endl
              << "Alias du Bateau :\t" << this->getAlias() << std::endl
              << "Taille du Bateau :\t" << this->getTaille() << std::endl
              << "Coordonnees du Bateau :\t" << this->getCoordonnees() << std::endl;
}
//
std::vector<short> Bateau::coordonneesConverties()
{
    return this->m_coordonnees->getCoordonneesToShort();
}
//
void Bateau::genererCoordonnees()
{
    this->m_coordonnees->genererCoordonnees(this->getTaille());
}
/*
 *  @name  :
 *  @param :
 *  @return:
 *  @desc  : méthode qui calcule la taille selon les coordonnées entrées de l'extérieur par l'utilisateur

*/
short  Bateau::tailleCalculee(const std::string& coordonnees_bateau)
{
    short taille(0);
    Coordonnees temp;
    
    if(Coordonnees::coordonneesSontValides(coordonnees_bateau))
    {
        temp.setCoordonnees(coordonnees_bateau);
        std::vector<short> coord = temp.getCoordonneesToShort();

        if(coord.at(0) == coord.at(2))                          //coordonnées de type H1H1 : position verticale
            taille = abs(coord.at(1) - coord.at(3)) + 1;    
        else if(coord.at(1) == coord.at(3))                     //coordonnées de type B7D7 : position horizontale
            taille = abs(coord.at(0) - coord.at(2)) + 1;
        else
            taille = 0;
    }                                                     
    return taille;
}