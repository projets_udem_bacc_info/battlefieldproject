battlefield:	battlefield.o	listeBateaux.o	grille.o	bateau.o	coordonnees.o
		g++	$(CFLAGS)	-o battlefield	battlefield.o	listeBateaux.o	grille.o	bateau.o	coordonnees.o

battlefield.o:	battlefield.cpp	listeBateaux.hpp	grille.hpp	bateau.hpp	coordonnees.hpp
		g++ $(CFLAGS)	-c	battlefield.cpp

coordonnees.o:	coordonnees.cpp	coordonnees.hpp
		g++	$(CFLAGS)	-c	coordonnees.cpp

bateau.o:	bateau.cpp	bateau.hpp	coordonnees.hpp
		g++	$(CFLAGS)	-c	bateau.cpp

listeBateaux.o:	listeBateaux.cpp	listeBateaux.hpp	bateau.hpp
		g++	$(CFLAGS)	-c	listeBateaux.cpp

grille.o:	grille.cpp	grille.hpp	bateau.hpp
		g++	$(CFLAGS)	-c	grille.cpp

clean:
		rm -f *.o battlefield