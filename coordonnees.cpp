/*
 * Cours:       INFO4008 : Programmation UNIX
 * 
 * Fichier contenant les fonctions qui nous permettrons de vérifier les coordonnées entré par l'utilisateur
 * Ou de générer automatiquement des coordonées valides qui seront utilisées par l'ordinateur (Processus)
 */
#include "coordonnees.hpp"
/*
 *  @name  :
 *  @param :
 *  @return:
 *  @desc  : initialisation du csteur
*/
Coordonnees::Coordonnees(void)
{
    this->m_correspondances = "ABCDEFGHIJ";
}
/*
 *  @name  :
 *  @param :
 *  @return:
 *  @desc  :
*/
Coordonnees::~Coordonnees(void){}
/*
 *  @name  :
 *  @param :
 *  @return:
 *  @desc  :
*/
std::string Coordonnees::getCoordonnees(void)
{
    return this->m_coordonnees;
}
/*
 *  @name  :
 *  @param :
 *  @return:
 *  @desc  : convertir les coordonnées du string au short
*/
std::vector<short>  Coordonnees::getCoordonneesToShort(void)
{
    std::vector<short> coordonnes_temporaire;
    std::string coordonnees = this->getCoordonnees();
    short position_x(0), position_y(0);
    // ici on va convertir en chiffre les coordonnées
    if(!coordonnees.empty())
    {
        for(int i(0); i < coordonnees.size(); i++)
        {
            // Le premier et 3 ème élément de nos coordonnées sont des lettre
            // on les isoles pour les convertir de lettre en chiffre
            if((i % 2) == 0)
            {
                position_x = this->lettreEnChiffre(coordonnees.at(i));
                // on diminue de 1 puisque sur la grille, le tableau commence avec 0
                coordonnes_temporaire.push_back(position_x--);
                position_x++; // on rétablit la position à la position intiale
            }
            else
            {
                position_y = coordonnees.at(i) - '0'; // convertit caractère en numérique
                coordonnes_temporaire.push_back(position_y);
            }
        }
        //this->coordonnees->set(this->getCoordonnees());
        //coordonnes_temporaire = this->coordonnees->ConvertirCoordoonnees();
    }
    return coordonnes_temporaire;
}
/*
 *  @name  :
 *  @param :
 *  @return:
 *  @desc  :
*/
void Coordonnees::setCoordonnees(const std::string& coordonnees){
    //mettre les lettre en majuscule
    this->m_coordonnees = coordonnees;
}
//
std::string Coordonnees::chiffreEnLettre(const short chiffre)
{
    std::string s(1, this->m_correspondances.at(chiffre));
    return s ;
    //return this->m_correspondances_coordonnees.at(chiffre);
}
//
int Coordonnees::lettreEnChiffre(const char lettre)
{
    int chiffre(0);
    std::string table_correspondance = this->m_correspondances;
    for(int i(0); i < table_correspondance.size(); i++)
    {
        if(table_correspondance.at(i) == lettre)
            chiffre = i;
    }
    return chiffre;
}
/*
 *  @name  :
 *  @param :
 *  @return:
 *  @desc  :Renvoie une génération automatique des coordonnées pour le jeu
*/
void Coordonnees::genererCoordonnees(short taille)
{
    std::string coordonnees_generee = "";
    if(taille > 0)
    {
        // va générer soit  0 : position verticale soit 
        //                  1 : position horizontale
        short position_du_bateau = rand() % 2;
        // nos coordonnées sont en composées de 2 paires de lettre et de chiffre : A0A0
        // la première paire(A0) Constitue le positionnement en X et Y du début du bateau
        // la deuxième paire(A0) constitue la fin du positionnement en X et Y du bateau
        short debut_x(0), debut_y(0), fin_x(0), fin_y(0);
        // on vérifie si l'alias du bateau et la taille sont déjà présents
       
        // Quelque soit la position du bateau les coordonnées début_x et début_x seront générée d'une seule manière
        // Ici les chiffre 0 à 9 correspondent à la table de correspondance des coordonnées
        // A:0 B:1 C:2 D:3 E:4 F:5 G:6 H:7 I:8 J:9
        debut_x = rand() % 10;
        debut_y = rand() % 10;
        // ensuite on va commencer à construire notre première partie de coordonnéees
        coordonnees_generee.append(chiffreEnLettre(debut_x));
        //coordonnees_generee.append(chiffreEnLettre(debut_y));
        coordonnees_generee.append(std::to_string(debut_y));
        // ensuite si la position du bateau est 0, on sait que le bateau va s'alligner
        // d'une manière verticale et pour ceux, debut_y et fin_y ont la même valeur
        // et début_x et fin_x ont des valeures différentes
        if (position_du_bateau == 0)
        {
            // on calcule la différence de valeur pour fin_x
            // en testant si on est hors limite de la grille de placement des bateaux
            if(debut_x < taille)
            {
                fin_x = debut_x + (taille - 1);
            }
            else
            {
                fin_x = debut_x - (taille - 1);
            }
            fin_y = debut_y;
            coordonnees_generee.append(chiffreEnLettre(fin_x));
            coordonnees_generee.append(std::to_string(fin_y));
            //coordonnees_generee.append(chiffreEnLettre(fin_y));
        }
        else
        {
            // si la position du bateau est 1 on sait que le bateau va s'aligner horizontalement
            // pour ceux, début_x et fin_x auront la même valeur
            // et début_y sera différent de fin_y
            if(debut_y < taille)
            {
                fin_y = debut_y + (taille - 1);
            }
            else
            {
                fin_y = debut_y - (taille - 1);
            }
            fin_x = debut_x;
            coordonnees_generee.append(chiffreEnLettre(fin_x));
            coordonnees_generee.append(std::to_string(fin_y));
        }
    }
    else
    {
        // coordonnees pour jeux
        short li(0), col(0);
        li  = rand() % 10;
        col = rand() % 10;
        coordonnees_generee.append(chiffreEnLettre(li));
        coordonnees_generee.append(std::to_string(col));
    }
    this->m_coordonnees = coordonnees_generee;
}
/*
 *  @name  :
 *  @param :
 *  @return:
 *  @desc  : vérifie si le format des coordoonnées entreé par le user
 *          est valide
*/
bool Coordonnees::coordonneesSontValides(const std::string& coordonnees)
{
    bool estValide = false;
    std::smatch m;

    if(coordonnees.empty())
    {
        estValide = false;
    }
    else if(TAILLE_COORD_BAT == coordonnees.size())
    {
        // est-ce que coord[0] && coord[2] sont des lettres? || coord[1] && coord[3] des chiffres?
        std::regex e("(([A-Ja-j][0-9]){2})");
        if(std::regex_search (coordonnees,m,e))
            estValide = true;
        else
            estValide = false;
    }
    else if(TAILLE_COORD_JEU == coordonnees.size())   // en plein jeu : taille = 2
    {
        std::regex e("([A-Ja-j][0-9])");              // est-ce que coord[0] est une lettre? || coord[1] est un chiffre?
        if(std::regex_search (coordonnees,m,e))
            estValide = true;
        else
            estValide = false;
    }
    else
    {
        estValide = false;
    }
    return estValide; 
}