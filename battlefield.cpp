#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <signal.h>

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cstring>

#include "bateau.hpp"
#include "listeBateaux.hpp"
#include "grille.hpp"
#include "coordonnees.hpp"

using namespace std;

#define TAMPON 250  // va servir de tampon lors de l'échange sur le pipe entre les deux utilisateurs
#define COORD   2   // taille de la coordonnées pendant le jeu

pid_t pid;          // défini le id des processus

void placer_bateaux_sur_grille_auto(ListeBateaux*, Grille*); // place les bateaux sur la grille avec coordonnees genere automatiquement
void placer_bateaux_sur_grille(ListeBateaux*, Grille*); // place les bateaux sur la grille avec coordonnees genere manuellement

void espace_de_jeux_pc_vs_user(Grille *pc, Grille *usr);  // fonction de jeu

int main()
{
    srand( time( NULL ) );

    cout << "\t*******************" << endl;
    cout << "\t* Bataille Navale *" << endl;
    cout << "\t*******************" << endl;
    cout << endl;

    // on commence par creer les deux grilles de jeux
    Grille *grille_pc   = new Grille();
    Grille *grille_user = new Grille();

    //------------- l'ordinateur va creer sa propre grille et placer ses bateaux dessus
    cout << "L'ordinateur va générer ses bateaux et créer sa grille, patientez ..." << endl;

    ListeBateaux *flotte_bateaux_pc = new ListeBateaux();
    flotte_bateaux_pc->genererBateaux(true);     // on va generer les bateaux de l'ordinateur automatiquement avec les coordonnees.

    placer_bateaux_sur_grille_auto(flotte_bateaux_pc, grille_pc );  // on essaie de placer les bateaux du pc sur la grille
    cout << "----> génération grille ordi et bateaux avec success !" << endl;
    grille_pc->voirGrille();
    // ------------ fin generation des bateaux de l'ordi et du placement sur la grille 

    /*  ------------ debut zone de creation de l'utilisateur et placement de ses bateaux sur la grille
        on affiche un menu a l'utilisateur pour lui demander s'il veut entrer les coordonnees manuellement
        ou s'il veut que l'ordinateur les genere automatiquement pour lui.
    */
    ListeBateaux *flotte_bateaux_user = new ListeBateaux();
    short choix_user(0);
    cout << "Veuillez choisir comment entrer vos coordonnees" << endl;
    cout << "1. Entrer manuellement vos coordonnees" << endl ;
    cout << "2. Generer automatiquement vos coordonnees" << endl ;
    cout << "Votre choix : "; cin >> choix_user;
    
    switch(choix_user)
    {
        case 1:
            flotte_bateaux_user->genererBateaux();
            placer_bateaux_sur_grille(flotte_bateaux_user, grille_user);
            break;
        case 2:
        default:
            flotte_bateaux_user->genererBateaux(true);
            placer_bateaux_sur_grille_auto(flotte_bateaux_user, grille_user);
            break;
    }
    cout << "----> génération grille de l'utilisateur et bateaux avec success !" << endl;
    // si tous se passe bien, on affiche au joueur le menu du jeux avec les instruction

    // ------------ fin generation des bateaux de l'utilisateur et du placement sur la grille 
    //grille_pc->voirGrille();
    //grille_user->voirGrille();
    flotte_bateaux_user->afficherBateaux();
    espace_de_jeux_pc_vs_user(grille_pc, grille_user);

    delete flotte_bateaux_user;
    delete flotte_bateaux_pc;
    delete grille_user;
    delete grille_pc;
    
    return 0;
}
// fonction qui va permettre de generer automatiquement les bateaux sur la grille de jeux
void placer_bateaux_sur_grille_auto(ListeBateaux *flotte_bateaux, Grille *grille_du_jeux)
{
    // on essaie de placer les bateaux sur la grille
    int nbre_bateaux = flotte_bateaux->getBateaux().size(); // nous donne le nombre total des bateaux dans la liste
    for(int i(0); i < nbre_bateaux;)
    {
        if(!grille_du_jeux->estLibre(flotte_bateaux->getBateaux().at(i))) // si il y a une collusion
        {
            flotte_bateaux->getBateaux().at(i)->genererCoordonnees(); // on genere a nouveau les coordonnees
        }
        else // si la grille est libre, on y place les bateaux
        {
            grille_du_jeux->placer(flotte_bateaux->getBateaux().at(i)); 
            i++;
        }
    }
}
// fonction qui va permettre de placer les bateaux sur la grille avec les coordonnees entre manuellement
void placer_bateaux_sur_grille(ListeBateaux *flotte_bateaux, Grille *grille_du_jeux)
{
    // on essaie de placer les bateaux sur la grille
    int nbre_bateaux = flotte_bateaux->getBateaux().size(); // nous donne le nombre total des bateaux dans la liste
    std::string coordonnees = "";

    for(int i(0); i < nbre_bateaux;)
    {
        cout << "Entrer les coordonnees du bateau : " << flotte_bateaux->getBateaux().at(i)->getAlias();
        cout << "( " << flotte_bateaux->getBateaux().at(i)->getTaille() << " )" << endl;
        cin  >> coordonnees;    // a faire : verifier si les coordonnees entree sont correcte !
        // on assigne les coordonnees au bateau
        if(flotte_bateaux->getBateaux().at(i)->getTaille() == Bateau::tailleCalculee(coordonnees))
        {
            flotte_bateaux->getBateaux().at(i)->setCoordonnees(coordonnees);
            // on verifie si la grille est libre pour les coordonnes du nouveau bateau
            if(grille_du_jeux->estLibre(flotte_bateaux->getBateaux().at(i)))
            {
                grille_du_jeux->placer(flotte_bateaux->getBateaux().at(i)); 
                i++;
            }
        }
        else
        {
            cout << "taille du bateau incorrecte !" << endl;
        }
    }
}
// méthode qui définit l'espace de jeux en séparant les processus
// la méthode va recevoir les deux grille de deux du pc et du user_vers_pc
void espace_de_jeux_pc_vs_user(Grille *pc, Grille *usr)
{
    // on définit le tampon par lequel les lectures se feront
    char tampon[TAMPON];
    // ensuite on définit les canaux vers lesquels les joueurs vont se transmettre les commandes de jeu
    std::string pc_vers_user = "", user_vers_pc = "";
    // creation des tubes aller-retour
    int user_vers_pc_pipe[2] = {0};
    int pc_vers_user_pipe[2] = {0};

    pipe(user_vers_pc_pipe); //on cree un tube de direction pour passer les info du processus de l'utilisateur vers le processus de l'ordinateur
    pipe(pc_vers_user_pipe); //on cree un tube de direction pour passer les info du processus de l'ordinateur vers le processus de l'utilisateur

    // on crée les processus
    // notre jeux consistera à faire intéragir 2 processus fils qui seront tous issus d'un même père
    Coordonnees coord_jeux;
    // echec du processus père
    if((pid = fork()) == -1)
    {
        exit(1);
    }// création processus père réussit
    else if(pid) 
    {   // si la création du second fils échoue
        if((pid = fork()) == -1)
        {
            exit(1);
        }
        else if(pid) // Processus père
        {
            // tant que les deux fils ne finiront pas leur exécution
            // le père continuera
            int c, w;
            wait(&c);
            wait(&w);
        }// processus fils 2 : ordinateur PC
        else
        {
            // on continuera à jouer tant que les bateaux sur la grille seront en service
            // si tous on encore une taille > 0
            // hile(pc->getTailleGrille() > 0)
            while(1)
            {
                
                // on ferme le canal d'écriture du user vers le pc afin de le lire
                close(user_vers_pc_pipe[1]);
                // on lit le canal de lecture du user vers le pc_to_user en mettant dans le tampon la commande recu
                int r = read(user_vers_pc_pipe[0], tampon, sizeof(tampon));
                //read(user_vers_pc_pipe[0], tampon, sizeof(tampon));
                // affichons le coordonnées : pour débogage
                user_vers_pc = tampon;
                cout << " FROM USER : " << user_vers_pc << endl;

                coord_jeux.setCoordonnees(user_vers_pc);
                
                pc->jouer(coord_jeux.getCoordonneesToShort(), "USER");
                pc->voirGrille();
                // avant de jouer il faut convertir les coordonnées
                close(pc_vers_user_pipe[0]);
                //après le jeu, le pc généere ses coordonnées pour envoyer vers le user_to_pc
                coord_jeux.setCoordonnees("");
                coord_jeux.genererCoordonnees();
                pc_vers_user = coord_jeux.getCoordonnees();
                // et on écrit vers le canal pour que le user le récupère
                write(pc_vers_user_pipe[1], pc_vers_user.c_str(), pc_vers_user.size()+1);
            }
            exit(EXIT_SUCCESS); // si le processus se termine normalement
        }
    }// processus fils 1 : utilisateur
    else
    {
        // même chose que du coté utilisateur
        //while(usr->getTailleGrille() > 0)
        while(1)
        {
            // on ferme le canal de lecture du user ver le pc pour faire sur que le processus pc ne lit pas dédans
            close(user_vers_pc_pipe[0]);
            cout << "Entrer une coordonnées : "; cin >> user_vers_pc;
            if(! Coordonnees::coordonneesSontValides(user_vers_pc))
            {
                cout << "Coordonnée invalide" << endl;
            }
            else
            {
                // on écrit dans le canal que le pc va lire
                write(user_vers_pc_pipe[1], user_vers_pc.c_str(), user_vers_pc.size()+1);
                // ensuite on lit ce que le pc nous a envoyer en fermant la lecture
                close(pc_vers_user_pipe[1]);
                // on lit ce que pc nous envoyé dans le tampon
                //read(pc_vers_user_pipe[0], tampon, sizeof(tampon));
                int r = read(pc_vers_user_pipe[0], tampon, sizeof(tampon));
                pc_vers_user = tampon;
                cout << "Coordonnées recu du pc : " << pc_vers_user << endl;
                // ensuite on joue sur la grille du user
                coord_jeux.setCoordonnees(pc_vers_user);

                usr->jouer(coord_jeux.getCoordonneesToShort(), "PC");
                // for(int i(0); i < TAMPON; ++i)
                //     tampon[i] = ' ';
            }
        }
        exit(EXIT_SUCCESS); // si le processus se termine normalement
    }
}