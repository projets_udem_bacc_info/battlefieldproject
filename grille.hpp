/*
 * Auteur:      Mukeya Arthson
 * Co-auteur:   Nombré Arnauld
 * Cours:       INFO4008 : Programmation UNIX
 * 
 * Fichier d'entete des fonctions qui nous permettrons de jouer sur la grille
 */
#ifndef GRILLE_H
#define GRILLE_H

#include <cstdlib>
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include "bateau.hpp"

using namespace std;

/*
    Classe Grille ...
*/
class Grille
{
public:
    Grille(void);              //csteur
    Grille(string);            //csteur
    virtual ~Grille(void);     //dsteur

    void    voirGrille();                       //méthode qui va afficher notre grille
    bool    estLibre(Bateau *);    //méthode pour vérifier si un bateau est déjà présent sur la grille
    void    placer(Bateau *);                  //ca sera l'objet bateau qu'elle prendra en parametre
    void    jouer(vector<short>, string);    //prendra les coordoonées qui lui sera envoyé et le string spécifie si c'est l'ordi ou le user
    void    verifierBateauxGrille(void);    //cette méthode vérifiera à chaque fois qu'un bateau sera touché
    short   getTailleGrille();              // taille de la grille
private:
    string  m_grille[10][10];           //notre grille est un tableau de 10 x 10
    short   m_taille_grille;            //au départ la grille contient 5 bateaux de taille 5 + 4 + 3 + 3 + 2 = 17
    map<string, short> m_bateau;          //string: nom & short: taille
    string m_valeursAxeVerticale;       // pour affichage à coté de la grille
};

#endif