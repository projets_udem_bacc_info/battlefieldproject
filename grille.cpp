/*
 * Auteur:      Mukeya Arthson
 * Co-auteur:   Nombré Arnauld
 * Cours:       INFO4008 : Programmation UNIX
 * 
 */
#include "grille.hpp"
#include "bateau.hpp"

/*
 *  @name  :
 *  @param :
 *  @return:
 *  @desc  : initialisation du csteur
*/
Grille::Grille(void)
{
    for(int i(0); i < 10; ++i)
    {
        for(int j(0); j < 10; ++j)
        {
            this->m_grille[i][j] = " ## ";      //exemple d'initialisation
        }
    }
    this->m_taille_grille = 17; // à changer afin de l'avoir automatiquement
    //this->m_bateau = {{" ", 0}};
    this->m_valeursAxeVerticale = "ABCDEFGHIJ";

}
/*
 *  @name  :
 *  @param :
 *  @return:
 *  @desc  :
*/
Grille::~Grille(void){}
/*
 *  @name  :
 *  @param :
 *  @return:
 *  @desc  :
*/
void Grille::voirGrille()
{
    cout << endl;
    cout << "    ";
    for(int i(0); i < 10; i++)
    {
        cout << i << "   ";
    }
    cout << endl << endl;
    for(int i(0); i < 10; ++i)
    {
        cout << this->m_valeursAxeVerticale.at(i) << "  ";
        for(int j(0); j < 10; ++j)
        {
            cout << this->m_grille[i][j];
        }
        cout << "    " << endl;
        cout << endl;
    }
}
/*
 *  @name  :
 *  @param :
 *  @return:
 *  @desc  :
*/
short Grille::getTailleGrille()
{
    return this->m_taille_grille;
}
/*
 *  @name  :
 *  @param : Adresse du bateau
 *  @return: void
 *  @desc  : Placer les bateaux sur la grille après avoir vérifié s'il n' y a aucune collusion
 *           les coordonnées seront sour la forme des entier par ex B7D7 = [1 7] [3 7] : format [li co].
 *           première paire : début, puis deuxième fin, ensuite on aura le sigle du bateau à afficher sur la grille
 *           Et la taille du bateau
*/
void Grille::placer(Bateau *bateau)
{

    short taille = bateau->getTaille();
    string nom = bateau->getAlias();
    vector<short> coord = bateau->coordonneesConverties();

    if(coord.at(0) == coord.at(2))          //coordonnées horizontales : type H7H8 = [7 7] [7 8]
    {
        for(int i(0); i < taille; ++i)
        {
            if(coord.at(1) < coord.at(3))   //affichage de gauche à droite |--->
                this->m_grille[coord.at(0)][coord.at(1)++] = " "+nom+" ";
            else                            //affichage de droite à gauche <---|
                this->m_grille[coord.at(0)][coord.at(1)--] = " "+nom+" ";
        }
    }
    else//(coord.at(1) == coord.at(3))      coordonnées verticales   : type B7D7 = [1 7] [3 7]
    {
        for(int i(0); i < taille; ++i)
        {
            if(coord.at(0) < coord.at(2))   //affichage de haut en bas |--->
                this->m_grille[coord.at(0)++][coord.at(1)] = " "+nom+" ";
            else                            //affichage du bas vers le haut <---|
                this->m_grille[coord.at(0)--][coord.at(1)] = " "+nom+" ";
        }
    }
    cout << "Bateau " << nom << " Placé sur la grille" << endl;
    //on garde les traces des bateaux entrés
    this->m_bateau.insert(pair<string, short>(nom, taille));
}
/*
 *  @name  :
 *  @param :
 *  @return: true s'il n' y a pas de collusion et false dans le cas contraire
 *  @desc  : vérifie avant de placer le nouveau bateau, s'il n' y a pas collusion
*/
bool Grille::estLibre(Bateau *bateau)
{
    int taille = bateau->getTaille();
    vector<short> coord = bateau->coordonneesConverties();

    int _cptr(0);

    if(coord.at(0) == coord.at(2))          //coordonnées horizontales : type H7H8 = [7 7] [7 8]
    {
        for(int i(0); i < taille; ++i)
        {
            if(coord.at(1) < coord.at(3))   //affichage de gauche à droite |--->
            {
                if(this->m_grille[coord.at(0)][coord.at(1)++] == " ## " )
                { 
                    _cptr++;
                }
            } 
            else                            //affichage de droite à gauche <---|
            {
                if(this->m_grille[coord.at(0)][coord.at(1)--] == " ## " )
                { 
                    _cptr++; 
                }
            }  
        }
    }
    else//(coord.at(1) == coord.at(3))        coordonnées verticales   : type B7D7 = [1 7] [3 7]
    {
        for(int i(0); i < taille; ++i)
        {
            if(coord.at(0) < coord.at(2))   //affichage de haut en bas |--->
            {
                if( this->m_grille[coord.at(0)++][coord.at(1)] == " ## ")
                { 
                    _cptr++; 
                }
            }
            else                            //affichage du bas vers le haut <---|
            {
                if(this->m_grille[coord.at(0)--][coord.at(1)] == " ## ")
                {
                     _cptr++; 
                }
            }
        }
    }
    //si le compteur == à la taille du bateau - c-à-d que la place est libre
    if(_cptr == taille)
        return true;
    else
    {
        cout << "Collusion" << endl;
        return false;
    }
}
/*
 *  @name  :
 *  @param :
 *  @return:
 *  @desc  : recoit les coordonnées numériques
*/
void Grille::jouer(vector<short> coord, string joueur)
{
    short li(0), col(0);
    //Bateau *bateau;
    //on recoit la paire des coordonnées
    li  = coord.at(0);
    col = coord.at(1);

    string nom_bateau = "";

    if(joueur == "PC")
        joueur = "USER : ";
    else
        joueur = "PC : ";
    //
    if(this->m_grille[li][col] == " ## ")       //veut dire que il n'y a rien donc à l'eau
    {
        cout << joueur << " A l'eau " << endl;
    }
    else if (this->m_grille[li][col] == " ** ") //si la position à déjà été touchée
    {
        cout << joueur << " A l'eau " << endl;
    }
    else                                        // si il y a un bateau présent, on le touche
    {
        cout << joueur << " Touché " << endl;
        //on récupère d'abord son nom, puis à travers le nom, on connait automatiquement sa taille
        nom_bateau = this->m_grille[li][col];
        // la taille du bateau diminue
        this->m_bateau[nom_bateau]--;
        // on vérifie si la taille du bateau est nulle 
        if(this->m_bateau[nom_bateau] == 0){
            cout << joueur << " Bateau " << nom_bateau << " Coulé " << endl;
        }
        this->m_grille[li][col] = " ** ";
        //on diminue aussi la taille de la grille
        this->m_taille_grille--;
    }
}