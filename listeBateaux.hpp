/*
 * Cours:       INFO4008 : Programmation UNIX
 *          Fichiers qui va contenir la définition des méthodes qui créer la liste des bateaux pour le jeux
 */
#ifndef LISTEBATEAUX_H
#define LISTEBATEAUX_H

#include "bateau.hpp"
#include <vector>
#include <map>

class ListeBateaux
{
public:
    ListeBateaux();         //cstor
    virtual ~ListeBateaux();// dsteur

    // méthodes de classes
    //void ajouter(Bateau *bateau);         // méthode qui va permettre d'ajouter une liste supplémentaire
    void genererBateaux(bool avec_ou_sansCoord = false); // méthode qui générer une liste automatiquemetn pour les bateaux par défaut
    //void enlever(Bateau *bateau);         // méthode pour enlever le bateau d'une liste
    std::vector<Bateau* > getBateaux() const;// méthode qui va contenir la liste des bateaux créé automatiquement
    void afficherBateaux()const;
private:
    //int m_nombre_bateau_par_defaut;
    bool m_deja_generer;
    std::vector<Bateau*> m_liste_bateaux;   // va contenir la liste des bateaux
    std::map<std::string, int> AliasTaille() const; // contiendra le tableau de correspondances des alias et taille pour les bateaux
};

#endif