/*
 * Cours:       INFO4008 : Programmation UNIX
 *          Fichiers qui va contenir l'implémentation des méthodes qui créer la liste des bateaux pour le jeux
 */
#include "listeBateaux.hpp"
//
ListeBateaux::ListeBateaux()
{
    //this->m_nombre_bateau_par_defaut = 5;
    this->m_deja_generer = false;
}
//
ListeBateaux::~ListeBateaux()
{
    // ici on vérifie si la liste des bateaux est vide ou pas
    // si oui, on éfface tous les bateaux créé
    if(this->m_liste_bateaux.size() > 0)
    {
        for(auto bateau : this->m_liste_bateaux)
        {
            delete bateau;
        }
        this->m_liste_bateaux.clear();
    }
}
//
void ListeBateaux::genererBateaux(bool avec_ou_sansCoord)
{
    //const int nombre_bateau = this->m_nombre_bateau_par_defaut;
    // eviter que le bateau soit générer plusieurs fois
    if(!this->m_deja_generer)
    {
        for(auto aliasTaille : AliasTaille())
        {
            this->m_liste_bateaux.push_back(new Bateau(aliasTaille.first, aliasTaille.second, false));
        }
        // on vérifie si le client veux avoir les coordonnées aussi automatiquement
        if(avec_ou_sansCoord)
        {
            for(auto i(0); i < this->m_liste_bateaux.size(); i++)
            {
                this->m_liste_bateaux.at(i)->genererCoordonnees();
            }
        }
        this->m_deja_generer = true;
    }  
}
// retourne la table de correspondance par défaut qui contient l'alias du bateau et sa taille par défaut
std::map<std::string, int> ListeBateaux::AliasTaille() const
{
    std::map<std::string, int> table;

    table.insert(std::pair<std::string, int>("PA", 5));
    table.insert(std::pair<std::string, int>("CS", 4));
    table.insert(std::pair<std::string, int>("SM", 3));
    table.insert(std::pair<std::string, int>("CT", 3));
    table.insert(std::pair<std::string, int>("TP", 2));

    return table;
}
// afficher les bateaux déja présents
void ListeBateaux::afficherBateaux()const
{
    if(this->m_liste_bateaux.size() != 0)
    {
        for(auto bateau : this->m_liste_bateaux)
        {
            bateau->details();
        }
    }
}
// liste des bateaux
std::vector<Bateau* > ListeBateaux::getBateaux() const
{
    return this->m_liste_bateaux;
}