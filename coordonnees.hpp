/*
 * Auteur:      Mukeya Arthson
 * Co-auteur:   Nombré Arnauld
 * Cours:       INFO4008 : Programmation UNIX
 * 
 * Fichier d'entete des fonctions qui nous permettrons de vérifier les coordonnées entré par l'utilisateur
 * Ou de générer automatiquement des coordonées valides qui seront utilisées par l'ordinateur (Processus)
 */
#ifndef COORDONNEES_H
#define COORDONNEES_H

#define TAILLE_COORD_BAT    4
#define TAILLE_COORD_JEU    2

#include <cstdlib>
#include <string>
#include <vector>
#include <iostream>
#include <ctime>
#include <regex>

/*
    Classe Coordonnees
    - Valide le format des coordonnées entré par l'utilisateur
    - Vérifie la taille(5,4,3,2,1) et la position des bateaux(horizontale ou verticale)
    - Converti les coordonnées du format chaine en format numérique pour leur placement sur la grille
*/
class Coordonnees
{
public:
    Coordonnees(void);              //csteur
    virtual ~Coordonnees(void);     //dsteur
    //accesseur et modificateur
    void    setCoordonnees(const std::string& coordonnees);
    std::string getCoordonnees();
    std::vector<short> getCoordonneesToShort();
    void genererCoordonnees(short taille = 0);
    static bool coordonneesSontValides(const std::string& coordonnees);

private:
    std::string chiffreEnLettre(const short chiffre);		// va nous renvoyer la valeur en Lettre d'un chiffre pour faire correspondre nos coordonnées														// en fonction de notre table de corespondance de coordonnées
	int lettreEnChiffre(const char lettre);
    std::string m_coordonnees; // va garder les coordoonnées en format chaine
    std::string m_correspondances;		//A:0 B:1 C:2 D:3 E:4 F:5 G:6 H:7 I:8 J:9
};

#endif 